#!/bin/bash

script_dir=$(dirname "$BASH_SOURCE")
source $script_dir/prepare.sh $1

rosservice call /$VEHICLE_NAME/fsm_node/set_state LANE_FOLLOWING
